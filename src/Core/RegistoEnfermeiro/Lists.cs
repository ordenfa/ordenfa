﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.RegistoEnfermeiro.Model;

namespace Core.RegistoEnfermeiro
{
    public static class Lists
    {
        public static Dictionary<NivelFormacao, string> NivelFormacaoDict { get; } = new Dictionary<NivelFormacao, string>
        { { NivelFormacao.Bacharel, "Bacharel" },
            { NivelFormacao.TecnicoBasico, "Técnico Básico" },
            { NivelFormacao.TecnicoMedio, "Técnico Médio" },
            { NivelFormacao.TecnicoMedioEspecializado, "Técnico Médio Especializado" },
            { NivelFormacao.TecnicoSuperior, "Técnico Superior" },
            { NivelFormacao.TecnicoSuperiorEspecializado, "Técnico Superior Especializado" }
        };

        public static IEnumerable<NivelFormacao> NivelFormacaoList { get; } = NivelFormacaoDict.Keys;

        public static string ToEnumString (this NivelFormacao that)
        {
            return NivelFormacaoDict[that];
        }

        public static Dictionary<Sexo, string> SexoDict { get; } = new Dictionary<Sexo, string>
        { { Sexo.Femenino, "Feminino" },
            { Sexo.Masculino, "Masculino" }
        };

        public static IEnumerable<Sexo> SexoList { get; } = SexoDict.Keys;

        public static string ToEnumString (this Sexo that)
        {
            return SexoDict[that];
        }

        public static Dictionary<Despacho, string> DespachoDict { get; } = new Dictionary<Despacho, string>
        { { Despacho.Autorizado, "Autorizado" },
            { Despacho.NaoAutorizado, "Não autorizado" }
        };

        public static IEnumerable<Despacho> DespachoList { get; } = DespachoDict.Keys;

        public static string ToEnumString (this Despacho that)
        {
            return DespachoDict[that];
        }

        public static Dictionary<EstadoCivil, string> EstadoCivilDict { get; } = new Dictionary<EstadoCivil, string>
        { { EstadoCivil.Solteiro, "Solteiro" },
            { EstadoCivil.Casado, "Casado" },
            { EstadoCivil.Divorciado, "Divorciado" },
            { EstadoCivil.Viuvo, "Viúvo" }
        };

        public static IEnumerable<EstadoCivil> EstadoCivilList { get; } = EstadoCivilDict.Keys;

        public static string ToEnumString (this EstadoCivil that)
        {
            return EstadoCivilDict[that];
        }

        public static Dictionary<Parecer, string> ParecerDict { get; } = new Dictionary<Parecer, string>
        { { Parecer.Aprovado, "Aprovado" },
            { Parecer.Reprovado, "Reprovado" }
        };

        public static IEnumerable<Parecer> ParecerList { get; } = ParecerDict.Keys;

        public static string ToEnumString (this Parecer that)
        {
            return ParecerDict[that];
        }
    }
}