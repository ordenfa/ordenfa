namespace Core.RegistoEnfermeiro.Model
{
    public enum Sexo
    {
        Masculino,
        Femenino
    }
    public enum EstadoCivil
    {
        Solteiro,
        Casado,
        Divorciado,
        Viuvo
    }
}