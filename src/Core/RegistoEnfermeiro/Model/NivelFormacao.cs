namespace Core.RegistoEnfermeiro.Model
{
    public enum NivelFormacao
    {
        TecnicoBasico,
        TecnicoMedio,
        TecnicoMedioEspecializado,
        Bacharel,
        TecnicoSuperior,
        TecnicoSuperiorEspecializado
    }
}