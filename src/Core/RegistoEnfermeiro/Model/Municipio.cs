﻿using System;

namespace Core.RegistoEnfermeiro.Model
{
    public class MunicipioFilter
    {
        public Guid? ProvinciaId { get; set; }
        public Guid? PaisId { get; set; }
        public bool? Nacional { get; set; }
    }
    public class Municipio
    {
        #region Propriedades
        public Guid Id { get; set; }
        public string Nome { get; private set; }
        public Guid ProvinciaId { get; private set; }
        public virtual Provincia Provincia { get; private set; }
        #endregion

        #region Constructores
        protected void NewEntity () => Id = Guid.NewGuid ();
        public Municipio () { }
        public Municipio (string nome, Provincia provincia) : this ()
        {
            NewEntity ();
            Nome = nome;
            Provincia = provincia;
            ProvinciaId = provincia.Id;
        }

        public void Alterar (string nome, Provincia provincia)
        {
            Nome = nome;
            Provincia = provincia;
            ProvinciaId = provincia.Id;
        }
        #endregion
    }
}