using System;

namespace Core.RegistoEnfermeiro.Model
{
    public class PaisFilter
    {
        public string Nome { get; set; }
    }
    public class Pais
    {
        #region Propriedades
        public Guid Id { get; set; }
        public string Nome { get; private set; }
        public string Nacionalidade { get; private set; }
        public string Codigo { get; private set; }
        public static Guid NacionalId => Guid.Parse ("E5093DFC-2505-4B82-A24B-6CD045879C62");
        #endregion

        #region Constructores
        protected void NewEntity () => Id = Guid.NewGuid ();
        public Pais () { }

        public Pais (string nome, string nacionalidade, string codigo) : this ()
        {
            NewEntity ();
            Nome = nome;
            Nacionalidade = nacionalidade;
            Codigo = codigo;
        }

        public void Alterar (string nome, string nacionalidade, string codigo)
        {
            Nome = nome;
            Nacionalidade = nacionalidade;
            Codigo = codigo;
        }
        #endregion
    }
}