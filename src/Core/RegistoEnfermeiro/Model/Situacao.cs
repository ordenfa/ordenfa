namespace Core.RegistoEnfermeiro.Model
{
    public enum Situacao
    {
        Activo,
        Desactivado
    }
}