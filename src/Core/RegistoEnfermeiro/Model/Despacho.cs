namespace Core.RegistoEnfermeiro.Model
{
    public enum Despacho
    {
        Autorizado,
        NaoAutorizado
    }
}