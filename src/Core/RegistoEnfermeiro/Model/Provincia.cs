﻿using System;
using System.Collections.Generic;

namespace Core.RegistoEnfermeiro.Model
{
    public class ProvinciaFilter
    {
        public Guid? PaisId { get; set; }
        public bool? Nacional { get; set; }
    }
    public class Provincia
    {
        #region Constructores
        protected void NewEntity () => Id = Guid.NewGuid ();
        public Provincia ()
        {
            Municipios = new HashSet<Municipio> ();
        }

        public Provincia (string nome, Pais pais) : this ()
        {
            NewEntity ();
            Nome = nome;
            Pais = pais;
            PaisId = pais.Id;
        }
        #endregion

        #region Propriedades
        public Guid Id { get; set; }
        public string Nome { get; private set; }
        public Guid PaisId { get; private set; }
        public virtual Pais Pais { get; private set; }
        public virtual ICollection<Municipio> Municipios { get; private set; }

        public void Alterar (string nome, Pais pais)
        {
            Nome = nome;
            Pais = pais;
            PaisId = pais.Id;
        }
        #endregion
    }
}