namespace Core.RegistoEnfermeiro.Model
{
    public enum Parecer
    {
        Aprovado,
        Reprovado
    }
}