using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Core.Financas.Model;

namespace Core.RegistoEnfermeiro.Model
{
    public class Enfermeiro
    {
        public Guid Id { get; set; }

        #region Geral
        public string Nome { get; set; }
        public string Pai { get; set; }
        public string Mae { get; set; }
        public DateTime? DataNascimento { get; set; }
        public Sexo? Sexo { get; set; }
        public EstadoCivil? EstadoCivil { get; set; }
        #endregion

        #region Localizacao
        public Guid? PaisNacionalidadeId { get; set; }
        public virtual Pais PaisNacionalidade { get; set; }
        public Guid? PaisOrigemId { get; set; }
        public virtual Pais PaisOrigem { get; set; }
        public Guid? ProvinciaOrigemId { get; set; }
        public virtual Provincia ProvinciaOrigem { get; set; }
        public Guid? MunicipioOrigemId { get; set; }
        public virtual Municipio MunicipioOrigem { get; set; }
        public string Naturalidade { get; set; }
        #endregion

        #region Identificacao
        public string NumeroDocumento { get; set; }
        public DateTime? DataEmissaoDocumento { get; set; }
        public string LocalEmissaoDocumento { get; set; }
        #endregion

        #region Endereco
        public string EnderecoMorada { get; set; }
        public string BairroMorada { get; set; }
        public Guid? ProvinciaMoradaId { get; set; }
        public virtual Provincia ProvinciaMorada { get; set; }
        public Guid? MunicipioMoradaId { get; set; }
        public virtual Municipio MunicipioMorada { get; set; }
        public string TelefonePessoal { get; set; }
        public string FaxPessoal { get; set; }
        public string EmailPessoal { get; set; }
        public string CaixaPostalPessoal { get; set; }
        #endregion

        #region Local de Trabalho
        public string EnderecoTrabalho { get; set; }
        public Guid? ProvinciaTrabalhoId { get; set; }
        public virtual Provincia ProvinciaTrabalho { get; set; }
        public Guid? MunicipioTrabalhoId { get; set; }
        public virtual Municipio MunicipioTrabalho { get; set; }
        public string TelefoneTrabalho { get; set; }
        #endregion

        #region Ordem
        public int NumeroOrdem { get; set; }
        public NivelFormacao? NivelFormacao { get; set; }
        public string EscolaUltimaFormacao { get; set; }
        public int? AnoUltimaFormacao { get; set; }
        public Guid? LocalRecepcaoId { get; set; }
        public virtual Provincia LocalRecepcao { get; set; }
        public DateTime? DataRecepcao { get; set; }
        public DateTime? DataRenovacao { get; set; }
        public DateTime? DataValidade { get; set; }
        public string Observacoes { get; set; }
        public NivelFormacao? NivelFormacaoAprovado { get; set; }
        public Parecer? ParecerComissaoAnalise { get; set; }
        public string ObservacaoComissaoAnalise { get; set; }
        public Despacho? DespachoBastonario { get; set; }
        public Situacao Situacao { get; set; } = Situacao.Activo;
        #endregion

        #region Relations
        public virtual ICollection<Cobranca> Cobrancas { get; set; } = new HashSet<Cobranca> ();
        #endregion

        #region Constructores
        public static Enfermeiro Novo (string nome, string pai, string mae, DateTime? dataNascimento, Sexo? sexo, EstadoCivil? estadoCivil)
        {
            var enfermeiro = new Enfermeiro ();
            enfermeiro.NewEntity ();
            enfermeiro.AlterarDadosPessoais (nome, pai, mae, dataNascimento, sexo, estadoCivil);
            enfermeiro.Situacao = Situacao.Activo;

            return enfermeiro;
        }
        #endregion

        #region Actualizar
        public void Desactivar ()
        {
            Situacao = Situacao.Desactivado;
        }

        public void Activar ()
        {
            Situacao = Situacao.Activo;
        }

        public void AlterarDadosPessoais (string nome, string pai, string mae, DateTime? dataNascimento, Sexo? sexo, EstadoCivil? estadoCivil)
        {
            Nome = nome;
            Pai = pai;
            Mae = mae;
            DataNascimento = dataNascimento;
            Sexo = sexo;
            EstadoCivil = estadoCivil;
        }

        public void AlterarOrigem (Pais paisNacionalidade, Pais paisOrigem, Provincia provinciaOrigem, Municipio municipioOrigem, string naturalidade)
        {
            PaisNacionalidade = paisNacionalidade;
            PaisNacionalidadeId = paisNacionalidade?.Id;
            PaisOrigem = paisOrigem;
            PaisOrigemId = paisOrigem?.Id;
            ProvinciaOrigem = provinciaOrigem;
            ProvinciaOrigemId = provinciaOrigem?.Id;
            MunicipioOrigem = municipioOrigem;
            MunicipioOrigemId = municipioOrigem?.Id;
            Naturalidade = naturalidade;
        }

        public void AlterarIdentificacao (string numero, DateTime? dataEmissao, string localEmissao)
        {
            NumeroDocumento = numero;
            DataEmissaoDocumento = dataEmissao;
            LocalEmissaoDocumento = localEmissao;
        }

        public void AlterarEnderecoMorada (string endereco, string bairro, Provincia provincia, Municipio municipio, string telefone, string email, string fax, string caixaPostal)
        {
            EnderecoMorada = endereco;
            BairroMorada = bairro;
            ProvinciaMorada = provincia;
            ProvinciaMoradaId = provincia?.Id;
            MunicipioMorada = municipio;
            MunicipioMoradaId = municipio?.Id;
            TelefonePessoal = telefone;
            EmailPessoal = email;
            FaxPessoal = fax;
            CaixaPostalPessoal = caixaPostal;
        }

        public void AlterarLocalTrabalho (string endereco, Provincia provincia, Municipio municipio, string telefone)
        {
            EnderecoTrabalho = endereco;
            ProvinciaTrabalho = provincia;
            ProvinciaTrabalhoId = provincia?.Id;
            MunicipioTrabalho = municipio;
            MunicipioTrabalhoId = municipio?.Id;
            TelefoneTrabalho = telefone;
        }

        public void AlterarDadosOrdem (int numero, NivelFormacao? nivelFormacao, string escolaUltimaFormacao, int? anoUltimaFormacao,
            Provincia localRecepcao, DateTime? dataRecepcao, string observacoes, NivelFormacao? nivelFormacaoAprovado, Parecer? parecerComissaoAnalise,
            string observacaoComissaoAnalise, Despacho? despachoBastonario)
        {
            NumeroOrdem = numero;
            NivelFormacao = nivelFormacao;
            EscolaUltimaFormacao = escolaUltimaFormacao;
            AnoUltimaFormacao = anoUltimaFormacao;
            LocalRecepcao = localRecepcao;
            LocalRecepcaoId = localRecepcao?.Id;
            DataRecepcao = dataRecepcao;
            Observacoes = observacoes;
            NivelFormacaoAprovado = nivelFormacaoAprovado;
            ParecerComissaoAnalise = parecerComissaoAnalise;
            ObservacaoComissaoAnalise = observacaoComissaoAnalise;
            DespachoBastonario = despachoBastonario;
        }

        protected void NewEntity () => Id = Guid.NewGuid ();
        #endregion
    }

    public class EnfermeiroFilter
    {
        public int? _Page { get; set; }
        public int? _Count { get; set; }
        public int? Situacao { get; set; }
        public int? NumeroOrdem { get; set; }
        public string Nome { get; set; }
        public string Pai { get; set; }
        public string NumeroDocumento { get; set; }
        public IEnumerable<Sexo> Sexo { get; set; } = Enumerable.Empty<Sexo> ();
        public IEnumerable<EstadoCivil> EstadoCivil { get; set; } = Enumerable.Empty<EstadoCivil> ();
        public IEnumerable<Guid> ProvinciaMoradaId { get; set; } = Enumerable.Empty<Guid> ();
        public IEnumerable<Guid> ProvinciaTrabalhoId { get; set; } = Enumerable.Empty<Guid> ();
        public IEnumerable<NivelFormacao> NivelFormacao { get; set; } = Enumerable.Empty<NivelFormacao> ();
    }

    public static class EnfermeiroFilterExtensions
    {
        public static IQueryable<Enfermeiro> Filter (this IQueryable<Enfermeiro> query, EnfermeiroFilter filter)
        {
            if (filter.NumeroOrdem != null)
            {
                query = query.Where (x => x.NumeroOrdem == filter.NumeroOrdem);
            }
            if (filter.Nome != null)
            {
                query = query.Where (x => x.Nome.ToLower ().Contains (filter.Nome.ToLower ()));
            }
            if (filter.NumeroDocumento != null)
            {
                query = query.Where (x => x.NumeroDocumento.ToLower ().Contains (filter.NumeroDocumento.ToLower ()));
            }
            if (filter.Sexo?.Any () ?? false)
            {
                query = query.Where (x => filter.Sexo.Contains (x.Sexo.Value));
            }
            if (filter.EstadoCivil?.Any () ?? false)
            {
                query = query.Where (x => filter.EstadoCivil.Contains (x.EstadoCivil.Value));
            }
            if (filter.NivelFormacao?.Any () ?? false)
            {
                query = query.Where (x => filter.NivelFormacao.Contains (x.NivelFormacao.Value));
            }
            if (filter.ProvinciaTrabalhoId?.Any () ?? false)
            {
                query = query.Where (x => filter.ProvinciaTrabalhoId.Contains (x.ProvinciaTrabalhoId.Value));
            }
            if (filter.ProvinciaMoradaId?.Any () ?? false)
            {
                query = query.Where (x => filter.ProvinciaMoradaId.Contains (x.ProvinciaMoradaId.Value));
            }

            return query;
        }

        public static IQueryable<Enfermeiro> Sort (this IQueryable<Enfermeiro> query, Sorter sorter)
        {
            Expression<Func<Enfermeiro, object>> exp = null;

            switch (sorter.Field)
            {
                case "nome":
                    exp = x => x.Nome;
                    break;
                case "pai":
                    exp = x => x.Pai;
                    break;
                case "numeroOrdem":
                    exp = x => x.NumeroOrdem;
                    break;
                case "numeroDocumento":
                    exp = x => x.NumeroDocumento;
                    break;
                case "nivelFormacao":
                    exp = x => x.NivelFormacao;
                    break;
                case "estadoCivil":
                    exp = x => x.EstadoCivil;
                    break;
                case "sexo":
                    exp = x => x.Sexo;
                    break;
                case "provinciaMoradaId":
                    exp = x => x.ProvinciaMorada.Nome;
                    break;
                case "municipioMoradaId":
                    exp = x => x.MunicipioMorada.Nome;
                    break;
            }

            if (sorter.Order == "ascend")
            {
                return exp == null ? query : query.OrderBy (exp);
            }
            else if (sorter.Order == "descend")
            {
                return exp == null ? query : query.OrderByDescending (exp);
            }

            return query;
        }

        public static IEnumerable<Enfermeiro> Search (this IQueryable<Enfermeiro> query, string term)
        {
            if (int.TryParse (term, out var numeroOrdem))
            {
                var resultNumeroOrdem = query.Where (x => x.NumeroOrdem == numeroOrdem);
                if (resultNumeroOrdem.Any ())
                {
                    return resultNumeroOrdem;
                }
            }

            var resultNumeroDocumento = query.Where (x => x.NumeroDocumento.ToLower () == term.ToLower ());
            if (resultNumeroDocumento.Any ())
            {
                return resultNumeroDocumento;
            }

            var resultNome = query.Where (x => x.Nome.ToLower ().Contains (term.ToLower ()));
            if (resultNome.Any ())
            {
                return resultNome;
            }

            return Enumerable.Empty<Enfermeiro> ();
        }
    }
}