﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Core.RegistoEnfermeiro.Model;
using Domain.Model.Inscricao;

namespace Core.Financas.Model
{
    public abstract class Cobranca
    {
        #region Propriedades
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public DateTime Data { get; set; }
        public decimal Valor { get; set; }
        public Guid EnfermeiroId { get; set; }
        public virtual Enfermeiro Enfermeiro { get; set; }
        public Guid? PagamentoId { get; set; }
        public virtual Pagamento Pagamento { get; set; }
        public EstadoCobranca Estado
        {
            get
            {
                if (Pagamento != null)
                {
                    return EstadoCobranca.Pago;
                }

                return EstadoCobranca.Pendente;
            }
        }
        #endregion

        #region Constructores
        protected Cobranca () { }

        protected Cobranca (Enfermeiro enfermiero, string descricao, DateTime data, decimal valor, Pagamento pagamento = null) : this ()
        {
            NewEntity ();
            Enfermeiro = enfermiero;
            EnfermeiroId = enfermiero.Id;
            Descricao = descricao;
            Data = data;
            Valor = valor;

            if (pagamento != null)
            {
                Pagar (pagamento);
            }
        }

        public void AlterarValor (decimal valor)
        {
            Valor = valor;
        }

        public void Pagar (Pagamento pagamento)
        {
            pagamento.Itens.Add (this);
            Pagamento = pagamento;
            PagamentoId = pagamento.Id;
        }

        protected void NewEntity () => Id = Guid.NewGuid ();
        #endregion
    }

    public class CobrancaFilter
    {
        public int? _Page { get; set; }
        public int? _Count { get; set; }
        public string Descricao { get; set; }
        public DateTime? Data { get; set; }
        public Guid? EnfermeiroId { get; set; }
        public Guid? PagamentoId { get; set; }
        public IEnumerable<EstadoCobranca> Estado { get; set; } = Enumerable.Empty<EstadoCobranca> ();
    }

    public static class CobrancaFilterExtensions
    {
        public static IQueryable<Cobranca> Filter (this IQueryable<Cobranca> query, CobrancaFilter filter)
        {
            if (filter.Data != null)
            {
                query = query.Where (x => x.Data == filter.Data);
            }

            if (filter.EnfermeiroId != null)
            {
                query = query.Where (x => x.EnfermeiroId == filter.EnfermeiroId);
            }

            if (filter.PagamentoId != null)
            {
                query = query.Where (x => x.PagamentoId == filter.PagamentoId);
            }

            if (filter.Estado.Any ())
            {
                query = query.Where (
                    x => filter.Estado.Any (
                        y => (y == EstadoCobranca.Pago && x.PagamentoId != null) ||
                        (y == EstadoCobranca.Pendente && x.PagamentoId == null)
                    )
                );
            }

            return query;
        }

        public static IQueryable<Cobranca> Sort (this IQueryable<Cobranca> query, Sorter sorter)
        {
            Expression<Func<Cobranca, object>> exp = null;
            Expression<Func<Cobranca, object>> exp2 = null;

            switch (sorter.Field)
            {
                case "data":
                    exp = x => x.Data;
                    break;
                case "descricao":
                    exp = x => x is Mensalidade ? ((Mensalidade) x).Ano : 1;
                    exp2 = x => x is Mensalidade ? ((Mensalidade) x).Mes : 1;
                    break;
                case "enfermeiroId":
                    exp = x => x.Enfermeiro.Nome;
                    break;
                case "estado":
                    exp = x => x.PagamentoId;
                    break;
                case "pagamentoId":
                    exp = x => x.PagamentoId;
                    break;
                case "valor":
                    exp = x => x.Valor;
                    break;
            }

            if (sorter.Order == "ascend")
            {
                query = exp == null ? query : query.OrderBy (exp);
                query = exp2 == null ? query : ((IOrderedQueryable<Cobranca>) query).ThenBy (exp2);
            }
            else if (sorter.Order == "descend")
            {
                query = exp == null ? query : query.OrderByDescending (exp);
                query = exp2 == null ? query : ((IOrderedQueryable<Cobranca>) query).ThenByDescending (exp2);
            }

            return query;
        }
    }
}