using System;
using System.Collections.Generic;
using System.Linq;
using Core.RegistoEnfermeiro.Model;
using Core.RegistoRH.Model;

namespace Core.Financas.Model
{
    public class Pagamento
    {
        public Guid Id { get; set; }
        public string Numero { get; set; }
        public virtual ICollection<Cobranca> Itens { get; set; }
        public DateTime Data { get; set; }
        public Guid UtilizadorId { get; set; }
        public virtual Utilizador Utilizador { get; set; }
        public Guid EnfermeiroId { get; set; }
        public virtual Enfermeiro Enfermeiro { get; set; }
        public decimal ValorTotal =>
            Itens.Select (x => x.Valor).Sum ();

        public Pagamento ()
        {
            Itens = new HashSet<Cobranca> ();
        }

        public Pagamento (string numero, Enfermeiro enfermeiro, DateTime data, Utilizador utilizador) : this ()
        {
            NewEntity ();
            Numero = numero;
            Enfermeiro = enfermeiro;
            EnfermeiroId = enfermeiro.Id;
            Data = data;
            Utilizador = utilizador;
            UtilizadorId = utilizador.Id;
        }

        protected void NewEntity () => Id = Guid.NewGuid ();
        #region Helpers
        public static string GerarNumero (DateTime data, string ultimoNumero)
        {
            var sequenciaAnual = ToBase36 (data.DayOfYear + 360);
            var sequenciaDiaria = "";

            var ano = data.ToString ("yy");

            if (ultimoNumero != null)
            {
                var ultimaSequenciaDiaria36 = ultimoNumero.Substring (ultimoNumero.Length - 2, 2);
                var ultimaSequenciaDiaria10 = FromBase36 (ultimaSequenciaDiaria36);
                var sequenciaDiaria10 = ultimaSequenciaDiaria10 + 1;

                sequenciaDiaria = ToBase36 (sequenciaDiaria10);
            }
            else
            {
                sequenciaDiaria = "01";
            }

            return "P" + ano + sequenciaAnual + sequenciaDiaria;

        }
        private const string charList = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private static string ToBase36 (int input)
        {
            var nbase = charList.Length;
            char[] clistarr = charList.ToCharArray ();
            var result = new Stack<char> ();
            while (input != 0)
            {
                result.Push (clistarr[input % nbase]);
                input /= nbase;
            }

            var r = new string (result.ToArray ());
            return r.PadLeft (2, '0');
        }

        private static int FromBase36 (string input)
        {
            var result = input
                .Reverse ()
                .Select ((c, i) => new
                {
                    Index = i,
                        Value = charList.IndexOf (c)
                })
                .Select (x => (int) Math.Pow (36, x.Index) * x.Value)
                .Sum ();

            return result;
        }
        #endregion
    }

    public class PagamentoFilter
    {
        public int? _Page { get; set; }
        public int? _Count { get; set; }
        public Guid? EnfermeiroId { get; set; }
    }
}