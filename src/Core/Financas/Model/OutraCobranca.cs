using System;
using Core.RegistoEnfermeiro.Model;
using Domain.Model.Inscricao;

namespace Core.Financas.Model
{
    public class OutraCobranca : Cobranca
    {
        public OutraCobranca ()
        {

        }
        public OutraCobranca (Enfermeiro enfermeiro, string descricao, DateTime data, decimal valor) : base (enfermeiro, descricao, data, valor)
        {

        }
    }
}