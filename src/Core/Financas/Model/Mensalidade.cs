using System;
using Core.RegistoEnfermeiro.Model;
using Domain.Model.Inscricao;

namespace Core.Financas.Model
{
    public class Mensalidade : Cobranca
    {
        #region Propriedades
        public int Ano { get; set; }
        public int Mes { get; set; }
        #endregion

        #region Constructores
        public Mensalidade () { }
        public Mensalidade (Enfermeiro enfermeiro, int ano, int mes, decimal valor) : base (enfermeiro, $"{ToParcelaString(ano, mes)}", new DateTime (ano, mes, 1), valor)
        {
            Ano = ano;
            Mes = mes;
        }
        #endregion

        public static string ToParcelaString (int ano, int mes)
        {
            var meses = new []
            {
                "Janeiro",
                "Fevereiro",
                "Março",
                "Abril",
                "Maio",
                "Junho",
                "Julho",
                "Agosto",
                "Setembro",
                "Outubro",
                "Novembro",
                "Dezembro"
            };

            var nomeMes = meses[mes - 1];
            return $"{nomeMes} de {ano}";
        }
    }
}