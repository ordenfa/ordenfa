namespace Domain.Model.Inscricao
{
    public enum EstadoCobranca
    {
        Pendente,
        Pago
    }
}