using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Core.RegistoEnfermeiro.Model;

namespace Core.Financas.Model
{
    public class CobrancaEnfermeiro
    {
        public string Nome { get; set; }
        public int NumeroOrdem { get; set; }
        public Guid EnfermeiroId { get; set; }
        public bool Devedor { get; set; }
        public decimal TotalDivida { get; set; }
        public decimal TotalPago { get; set; }
        public int MesesDivida { get; set; }
        public string TelefonePessoal { get; set; }
        public Guid? ProvinciaMoradaId { get; set; }
        public string ProvinciaMorada { get; set; }
    }

    public class CobrancaEnfermeiroFilter
    {
        public int? _Page { get; set; }
        public int? _Count { get; set; }
        public IEnumerable<bool> Devedor { get; set; }
        public IEnumerable<Guid> ProvinciaMoradaId { get; set; }
    }

    public static class CobrancaEnfermeiroExtensions
    {
        public static IQueryable<CobrancaEnfermeiro> CobrancaEnfermeiros (this IQueryable<Enfermeiro> query)
        {
            var hoje = DateTime.Now;
            var result = query.Select (x => new CobrancaEnfermeiro
            {
                EnfermeiroId = x.Id,
                    Nome = x.Nome,
                    NumeroOrdem = x.NumeroOrdem,
                    TelefonePessoal = x.TelefonePessoal,
                    ProvinciaMoradaId = x.ProvinciaMoradaId,
                    ProvinciaMorada = x.ProvinciaMorada.Nome,
                    Devedor = x.Cobrancas.OfType<Mensalidade> ()
                    .Where (y => y.PagamentoId == null)
                    .Any (y => hoje.Year > y.Ano || (hoje.Year == y.Ano && hoje.Month > y.Mes)),
                    MesesDivida = x.Cobrancas.OfType<Mensalidade> ()
                    .Where (y => y.PagamentoId == null)
                    .Count (y => hoje.Year > y.Ano || (hoje.Year == y.Ano && hoje.Month > y.Mes)),
                    TotalDivida = x.Cobrancas.OfType<Mensalidade> ()
                    .Where (y => y.PagamentoId == null)
                    .Where (y => hoje.Year > y.Ano || (hoje.Year == y.Ano && hoje.Month > y.Mes))
                    .Sum (y => y.Valor),
                    TotalPago = x.Cobrancas.OfType<Mensalidade> ()
                    .Where (y => y.PagamentoId != null)
                    .Sum (y => y.Valor)
            });

            return result;
        }

        public static IQueryable<CobrancaEnfermeiro> Filter (this IQueryable<CobrancaEnfermeiro> query, CobrancaEnfermeiroFilter filter)
        {
            if (filter.Devedor != null && filter.Devedor.Any ())
            {
                query = query.Where (x => filter.Devedor.Contains (x.Devedor));
            }

            if (filter.ProvinciaMoradaId != null && filter.ProvinciaMoradaId.Any ())
            {
                query = query.Where (x => filter.ProvinciaMoradaId.Contains (x.ProvinciaMoradaId.Value));
            }

            return query;
        }

        public static IQueryable<CobrancaEnfermeiro> Sort (this IQueryable<CobrancaEnfermeiro> query, Sorter sorter)
        {
            Expression<Func<CobrancaEnfermeiro, object>> exp = null;
            Expression<Func<CobrancaEnfermeiro, object>> exp2 = null;

            switch (sorter.Field)
            {
                case "nome":
                    exp = x => x.Nome;
                    break;
                case "numeroOrdem":
                    exp = x => x.NumeroOrdem;
                    break;
                case "devedor":
                    exp = x => x.Devedor;
                    break;
                case "mesesDivida":
                    exp = x => x.MesesDivida;
                    break;
                case "totalDivida":
                    exp = x => x.TotalDivida;
                    break;
                case "totalPago":
                    exp = x => x.TotalPago;
                    break;
                case "provinciaMoradaId":
                    exp = x => x.ProvinciaMorada;
                    break;
            }

            if (sorter.Order == "ascend")
            {
                query = exp == null ? query : query.OrderBy (exp);
                query = exp2 == null ? query : ((IOrderedQueryable<CobrancaEnfermeiro>) query).ThenBy (exp2);
            }
            else if (sorter.Order == "descend")
            {
                query = exp == null ? query : query.OrderByDescending (exp);
                query = exp2 == null ? query : ((IOrderedQueryable<CobrancaEnfermeiro>) query).ThenByDescending (exp2);
            }

            return query;
        }
    }
}