using System;
using System.Collections.Generic;
using System.Linq;
namespace Core
{

    public class ListValue
    {
        public string Description { get; set; }
        public string Value { get; set; }
    }

    public static class ListValueExtensions
    {
        public static IEnumerable<ListValue> ToListValueList<TEnum> (this Dictionary<TEnum, string> dict) =>
            dict.Select (x => new ListValue ()
            {
                Value = x.Key.ToString (),
                    Description = x.Value,
            });
    }

    public class Sorter
    {
        public string Field { get; set; }
        public string Order { get; set; }
    }
}