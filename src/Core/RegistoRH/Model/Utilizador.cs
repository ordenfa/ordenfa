using System;
using System.Collections.Generic;

namespace Core.RegistoRH.Model
{
    public class UtilizadorFilter
    {
        public string Nome { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public Guid? RoleId { get; set; }
        public int? _Page { get; set; }
        public int? _Count { get; set; }
    }
    public class Utilizador
    {
        #region Propriedades
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public Guid RoleId { get; set; }
        public virtual Role Role { get; set; }
        #endregion

        #region Constructores
        public static Utilizador NewUtilizador (string nome, string userName, string email, Role role)
        {
            var utilizador = new Utilizador ();
            utilizador.NewEntity ();
            utilizador.Nome = nome;
            utilizador.UserName = userName;
            utilizador.Email = email;
            utilizador.RoleId = role.Id;
            utilizador.Role = role;

            return utilizador;
        }

        protected void NewEntity () => Id = Guid.NewGuid ();
        #endregion
    }
}