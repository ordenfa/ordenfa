using System;

namespace Core.RegistoRH.Model
{
    public class Role
    {
        #region Propriedades
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        #endregion

        #region Constructores
        public static Role NewRole (string nome, string codigo)
        {
            var role = new Role ();
            role.NewEntity ();
            role.Nome = nome;
            role.Codigo = codigo;

            return role;
        }

        protected void NewEntity () => Id = Guid.NewGuid ();
        #endregion
    }
}