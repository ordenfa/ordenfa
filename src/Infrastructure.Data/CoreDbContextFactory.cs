﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.Data
{
    public class CoreDbContextFactory : IDesignTimeDbContextFactory<CoreDbContext>
    {
        public CoreDbContext CreateDbContext (string[] args)
        {
            var configurationBuilder = new ConfigurationBuilder ();
            // .SetBasePath (Environment.CurrentDirectory)
            // .AddJsonFile ("appsettings.json");

            var configuration = configurationBuilder.Build ();

            var provider = configuration["Data:DefaultConnection:Provider"];
            var connectionString = configuration["Data:DefaultConnection:ConnectionString"];
            provider = "PostgreSql";
            connectionString = "Host=localhost;Port=5432;Database=ordenfa;Username=postgres;Password=postgres";

            var optionsBuilder = new DbContextOptionsBuilder<CoreDbContext> ();

            switch (provider)
            {
                case "Sqlite":
                    optionsBuilder.UseSqlite (connectionString);
                    break;
                    // case "PostgreSql":
                    //     optionsBuilder.UseNpgsql (connectionString);
                    //     break;
                case "SqlServer":
                    optionsBuilder.UseSqlServer (connectionString);
                    break;
                default:
                    throw new Exception ("Erro de provider");
            }

            return new CoreDbContext (optionsBuilder.Options);
        }
    }
}