using Core.RegistoEnfermeiro.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.EntityConfigurations
{
    public class EnfermeiroEntityTypeConfiguration : IEntityTypeConfiguration<Enfermeiro>
    {
        public void Configure (EntityTypeBuilder<Enfermeiro> builder)
        {
            builder.ToTable ("Enfermeiros");
            builder
                .HasIndex (x => x.NumeroOrdem)
                .IsUnique ();

            builder
                .HasIndex (x => x.NumeroDocumento)
                .IsUnique ();

            builder
                .HasMany (x => x.Cobrancas)
                .WithOne (x => x.Enfermeiro)
                .OnDelete (DeleteBehavior.Cascade);
        }
    }
}