﻿using Core.Financas.Model;
using Core.RegistoEnfermeiro.Model;
using Core.RegistoRH.Model;
using Infrastructure.Data.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class CoreDbContext : DbContext
    {
        #region Entities
        public DbSet<Utilizador> Utilizadores { get; set; }
        public DbSet<Enfermeiro> Enfermeiros { get; set; }
        public DbSet<Pais> Paises { get; set; }
        public DbSet<Provincia> Provincias { get; set; }
        public DbSet<Municipio> Municipios { get; set; }

        public DbSet<Cobranca> Cobrancas { get; set; }
        public DbSet<Mensalidade> Mensalidades { get; set; }
        public DbSet<OutraCobranca> OutrasCobrancas { get; set; }
        public DbSet<Pagamento> Pagamentos { get; set; }
        #endregion

        #region Constructors
        public CoreDbContext (DbContextOptions<CoreDbContext> options) : base (options) { }
        #endregion

        #region Metodos
        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder)
        {
            // optionsBuilder
            //     .UseLazyLoadingProxies ();
        }

        protected override void OnModelCreating (ModelBuilder builder)
        {
            builder.ApplyConfiguration (new EnfermeiroEntityTypeConfiguration ());
            builder.Entity<Pagamento> ().ToTable ("Pagamento");
            base.OnModelCreating (builder);
        }
        #endregion
    }
}