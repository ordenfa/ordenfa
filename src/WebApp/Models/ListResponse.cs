using System.Collections.Generic;

namespace WebApp.Models
{
    public class ListResponse<TItem>
    {
        public int Total { get; set; }
        public IEnumerable<TItem> Items { get; set; }
    }
}