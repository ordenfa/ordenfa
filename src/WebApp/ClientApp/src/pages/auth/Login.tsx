import React from 'react';
import { LoginActions } from '@/utils/ApiAuthorizationConstants';
import LoginBase from './components/LoginBase';

export default function Login() {
  return <LoginBase action={LoginActions.Login} />;
}
