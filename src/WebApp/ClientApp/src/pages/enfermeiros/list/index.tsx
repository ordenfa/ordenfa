/*
 * name: list
 * icon: file-search
 * authority: LEITOR
 */
import React, { useState, useRef } from 'react';
import { Card } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

import { Button, Drawer, Tag } from 'antd';
import ProTable, { ProColumns, TableDropdown, ActionType } from '@ant-design/pro-table';
import { PlusOutlined } from '@ant-design/icons';
import request from '@/utils/request';

// import styles from './style.less';
// import CreateButton from './components/CreateButton';
// import { PaginationConfig, SorterResult } from 'antd/lib/table';
// import { EnfermeiroListItem } from '../components/types';
// import { useEnfermeirosTable } from '../components/state';
// import { setColumnSearchProps } from '@/utils/table-antd';
// import { useColumns } from './components/columns';
// import { toEnfermeiroFilters, toEnfermeiroSorters } from './components/mappers';
// import TableForm from './components/TableForm';
// import DownloadButton from './components/DownloadButton';
// import axios from 'axios';
// import qs from 'qs';

type ListResponse<TItem> = {
  total: number;
  items: TItem[];
};

type EnfermeiroListItem = {
  id: string;
  nome: string;
  numeroOrdem: string;
  nacionalidadeId?: string;
  paisNacionalidade?: {
    nacionalidade: string;
  };
};

const columns: ProColumns<EnfermeiroListItem>[] = [
  {
    dataIndex: 'numeroOrdem',
    title: 'Número de ordem',
    copyable: true,
  },
  {
    dataIndex: 'nome',
    title: 'Nome',
  },
  {
    dataIndex: 'paisNacionalidade.nacionalidade',
    title: 'Nacionalidade',
  },
  {
    title: '',
    valueType: 'option',
    dataIndex: 'id',
    render: (text, row, _, action) => [
      <TableDropdown
        onSelect={() => action.reload()}
        menus={[
          { key: 'copy', name: 'CP' },
          { key: 'delete', name: 'DL' },
        ]}
      />,
    ],
  },
];

export default () => {
  const actionRef = useRef<ActionType>();
  const [visible, setVisible] = useState(false);

  return (
    <>
      <Drawer onClose={() => setVisible(false)} visible={visible}>
        <Button
          style={{
            margin: 8,
          }}
          onClick={() => {
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }}
        >
          OK
        </Button>
        <Button
          onClick={() => {
            if (actionRef.current) {
              actionRef.current.reset();
            }
          }}
        >
          Limpar
        </Button>
      </Drawer>
      <ProTable<EnfermeiroListItem>
        columns={columns}
        actionRef={actionRef}
        request={async (params = {}) => {
          const data = await request<ListResponse<EnfermeiroListItem>>('/api/enfermeiro', {
            params: {
              ...params,
              page: params.current,
              per_page: params.pageSize,
            },
          });

          return {
            data: data.items,
            page: params.current,
            success: true,
            total: data.total,
          };
        }}
        rowKey="id"
        pagination={{
          showSizeChanger: true,
        }}
        dateFormatter="string"
        headerTitle="Enfermeiros"
        params={{ state: 'all' }}
        toolBarRender={() => [
          <Button key="3" type="primary" onClick={() => setVisible(true)}>
            <PlusOutlined />
            Adicionar
          </Button>,
        ]}
      />
    </>
  );
};

/*
export default function EnfermeiroListPage() {
  const { data, dispatch } = useEnfermeirosTable({
    sorterArgs: { field: 'nome', order: 'ascend' },
  });
  const { columns, visibleColumns, setVisibleColumns } = useColumns();
  const [filter, setFilter] = useState({});

  const handleStandardTableChange = (
    pagination: PaginationConfig,
    filterObj: Record<keyof EnfermeiroListItem, string[]>,
    sorter: SorterResult<EnfermeiroListItem>,
  ) => {
    dispatch({
      type: 'refetch',
      paginationArgs: {
        pageNumber: pagination.current,
        pageSize: pagination.pageSize,
      },
      filterArgs: toEnfermeiroFilters(filterObj),
      sorterArgs: toEnfermeiroSorters(sorter),
    });

    setFilter(filterObj);
  };

  const onVisibleColumnsChange = (values: (string | number)[]) => {
    setVisibleColumns(values as any);
  };

  const footer = () => {
    return (
      <div>
        Total encontrado: <b>{data.pagination.total}</b>
      </div>
    );
  };

  return (
    <PageHeaderWrapper
      title="Pesquisar Enfermeiros"
      content={<TableForm onChange={onVisibleColumnsChange} visibleColumns={visibleColumns} />}
      extra={[<DownloadButton href={makeReportUri(filter)} />]}
    >
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            <CreateButton />
          </div>
          <StandardTable
            bordered
            loading={data.loading}
            data={data}
            columns={columns}
            onChange={handleStandardTableChange}
            scroll={{ x: 1200 }}
            simple
            footer={footer}
          />
        </div>
      </Card>
    </PageHeaderWrapper>
  );
}
*/

/*
function makeReportUri(values: any, sorter: any = undefined) {
  const pathBase = `${process.env.API_URL}/extract`;
  const tipoRelatorio = 'TodosEnfermeiros';

  // @ts-ignore
  return axios.getUri({
    method: 'get',
    url: `${pathBase}/${tipoRelatorio}`,
    params: {
      ...(sorter && sorter.field ? { _sorter: `${sorter.field}_${sorter.order}` } : {}),
      ...values,
    },
    paramsSerializer: function(params) {
      return qs.stringify(params, { arrayFormat: 'repeat' });
    },
  });
}
*/
