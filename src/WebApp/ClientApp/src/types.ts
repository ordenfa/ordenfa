import { Profile } from 'oidc-client';

export type InitialState = {
  user: Profile | null;
};
