import { extend } from 'umi-request';
import { notification } from 'antd';
import { getAuthorizationHeader } from './auth';

const codeMessage = {
  200: 'Erro desconhecido',
};

const errorHandler = (error: any) => {
  const { response = {} } = error;
  const errortext = response.statusText; // codeMessage[response.status] || response.statusText;
  const { status, url } = response;

  notification.error({
    message: `Erro ${status}: ${url}`,
    description: errortext,
  });
};

const request = extend({
  errorHandler,
  credentials: 'include',
});

request.use(async (ctx, next) => {
  const { req } = ctx;
  const { url, options } = req;

  if (url.startsWith('/api')) {
    const authorization = await getAuthorizationHeader();
    const headers = {
      ...(options.headers || {}),
      ...(authorization ? { authorization } : {}),
    };

    ctx.req.options = {
      ...options,
      headers,
    };
  }

  await next();
});

export default request;
