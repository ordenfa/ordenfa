import React from 'react';
import ProLayout from '@ant-design/pro-layout';
import RightContent from './components/RightContent';

const AppLayout: React.FC = props => {
  return (
    <ProLayout rightContentRender={RightContent} title="Ordenfa">
      {props.children}
    </ProLayout>
  );
};

export default AppLayout;
