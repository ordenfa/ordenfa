import React from 'react';
import SelectLang from './SelectLang';
import styles from './RightContent.less';
import AvatarDropdown from './AvatarDropdown';

export type SiderTheme = 'light' | 'dark';
export interface RightContentProps {
  theme?: SiderTheme;
  layout?: 'sidemenu' | 'topmenu';
}

const RightContent: React.SFC<RightContentProps> = props => {
  const { theme, layout = 'sidemenu' } = props;
  let className = styles.right;

  if (theme === 'dark' && layout === 'topmenu') {
    className = `${styles.right}  ${styles.dark}`;
  }

  return (
    <div className={className}>
      <SelectLang className={styles.action} />
      <AvatarDropdown menu />
    </div>
  );
};

export default RightContent;
