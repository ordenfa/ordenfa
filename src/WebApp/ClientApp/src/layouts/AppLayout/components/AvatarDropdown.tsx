import { Avatar, Menu, Spin } from 'antd';
import { ClickParam } from 'antd/es/menu';
import { LogoutOutlined } from '@ant-design/icons';
import { FormattedMessage } from 'umi-plugin-react/locale';
import React from 'react';
import router from 'umi/router';

import HeaderDropdown from './HeaderDropdown';
import styles from './RightContent.less';
import { useAccess } from '@/pages/.umi/plugin-access/access';
import { logout } from '@/utils/auth';

export interface GlobalHeaderRightProps {
  menu?: boolean;
}

export default function AvatarDropdown(props: GlobalHeaderRightProps) {
  const { menu } = props;
  const { user: currentUser } = useAccess();

  const onMenuClick = (event: ClickParam) => {
    const { key } = event;

    if (key === 'logout') {
      return logout();
    }

    router.push(`/account/${key}`);
  };

  if (!menu) {
    return (
      <span className={`${styles.action} ${styles.account}`}>
        <Avatar
          size="small"
          className={styles.avatar}
          alt="avatar"
          style={{ backgroundColor: '#87d068' }}
          icon="user"
        />
        <span className={styles.name}>{currentUser?.name}</span>
      </span>
    );
  }

  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
      {/* <Menu.Item key="center">
        <Icon type="user" />
        <FormattedMessage id="menu.account.center" defaultMessage="account center" />
      </Menu.Item>
      <Menu.Item key="settings">
        <Icon type="setting" />
        <FormattedMessage id="menu.account.settings" defaultMessage="account settings" />
      </Menu.Item>
      <Menu.Divider /> */}
      <Menu.Item key="logout">
        <LogoutOutlined />
        <FormattedMessage id="menu.account.logout" defaultMessage="logout" />
      </Menu.Item>
    </Menu>
  );

  return currentUser ? (
    <HeaderDropdown overlay={menuHeaderDropdown}>
      <span className={`${styles.action} ${styles.account}`}>
        <Avatar
          size="small"
          className={styles.avatar}
          alt="avatar"
          style={{ backgroundColor: '#87d068' }}
          icon="user"
        />
        <span className={styles.name}>{currentUser?.name}</span>
      </span>
    </HeaderDropdown>
  ) : (
    <Spin size="small" style={{ marginLeft: 8, marginRight: 8 }} />
  );
}
