import React from 'react';
import AppLayout from './AppLayout';
import BlankLayout from './BlankLayout';
import { RouteComponentProps } from 'dva/router';

const Layout: React.FC<RouteComponentProps> = props => {
  const url = props.location.pathname;

  if (url.startsWith('/auth/')) {
    return <BlankLayout {...props} />;
  }

  return <AppLayout {...props} />;
};

export default Layout;
