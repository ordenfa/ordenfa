import authService from '@/utils/auth';
import { InitialState } from './types';

export async function getInitialState(): Promise<InitialState> {
  const user = await authService.getUser();

  console.log(user);
  return {
    user,
  };
}
