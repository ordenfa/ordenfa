import { InitialState } from './types';

export default function(initialState: InitialState) {
  const { user } = initialState; // the initialState is the returned value in step 2
  console.log(user);
  return {
    user,
    authenticated: !!user,
    leitor: true,
    editor: true,
    financeiro: true,
    admin: true,
  };
}
