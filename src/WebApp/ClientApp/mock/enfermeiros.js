export default {
  // supported values are Object and Array
  'GET /api/enfermeiros': {
    users: [
      {
        total: 4,
        items: [
          {
            id: 1,
            numeroOrdem: '1234',
            nome: 'Leza L',
          },
          {
            id: 2,
            numeroOrdem: '1235',
            nome: 'Frida Z',
          },
          {
            id: 3,
            numeroOrdem: '1236',
            nome: 'Mercy ZL',
          },
          {
            id: 4,
            numeroOrdem: '1237',
            nome: 'Next ZL',
          },
        ],
      },
    ],
  },

  // GET and POST can be omitted
  '/api/users/1': { id: 1 },

  // support for custom functions, APIs refer to express@4
  'POST /api/users/create': (req, res) => {
    res.end('OK');
  },
};
