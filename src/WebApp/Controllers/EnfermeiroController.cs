using Bogus;
using Core.RegistoEnfermeiro.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize]
    [ApiController]
    [Route ("api/[controller]")]
    public class EnfermeiroController : ControllerBase
    {
        private readonly ILogger<EnfermeiroController> _logger;

        public EnfermeiroController (ILogger<EnfermeiroController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ListResponse<Enfermeiro> Get ()
        {
            var enfermeiros = new Faker<Enfermeiro> ()
                .RuleFor (x => x.Id, f => f.Random.Guid ())
                .RuleFor (x => x.NumeroOrdem, f => f.Random.Number (1, int.MaxValue))
                .RuleFor (x => x.Nome, f => f.Person.FullName)
                .RuleFor (x => x.PaisNacionalidade, f => new Pais (f.Address.Country (), f.Address.Country (), f.Address.CountryCode ()))
                .GenerateLazy (145);

            return new ListResponse<Enfermeiro> ()
            {
                Total = 145,
                    Items = enfermeiros
            };
        }
    }
}